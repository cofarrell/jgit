package org.eclipse.jgit.internal.storage.file;

import org.eclipse.jgit.lib.*;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.treewalk.AbstractTreeIterator;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

public class PackIndexV22Test {

    public static final File LINUX = new File("/Users/charleso/src/github/linux/.git");
    public static final File JIRA = new File("/Users/charleso/Desktop/jira-deleteme");

    //    @Test
    public void testLargePack() throws Exception {
        new Timed<Void>("large linux pack", 1000) {
            @Override
            protected Void iteration() throws Exception {
                PackIndex open = PackIndex.open(new File("/Users/charleso/src/github/linux/.git/objects/pack/pack-73d6171b9aa2d73bc0c58e28ff8c753d53c35957.idx"));
//                open.findOffset(ObjectId.fromString("0c14e5ced264620284bd96f888614768c9bd0976"));
                return null;
            }
        }.start(false);
    }

    @Test
    public void testFullLazy() throws Exception {

        new Timed<String>("large full linux pack", 10000) {
            @Override
            protected String iteration() throws Exception {
                return type(new File("/Users/charleso/src/github/linux/.git"), "0c14e5ced264620284bd96f888614768c9bd0976", "drivers/net/ethernet/broadcom/bnx2x/bnx2x_cmn.c");
            }
        }.start(true);
    }

    @Test
    public void testFull2() throws Exception {
        new Timed<String>("large full linux pack", 10000) {
            @Override
            protected String iteration() throws Exception {
                return type(LINUX, "0c14e5ced264620284bd96f888614768c9bd0976", "drivers/net/ethernet/broadcom/bnx2x/bnx2x_cmn.c");
            }
        }.start(false);
    }


    @Test
    public void testPerfDirectoryLazy() throws Exception {
        new Timed<String>("directory lazy", 10000) {
            @Override
            protected String iteration() throws Exception {
                return directory(LINUX, "2e0cbf2cc2c9371f0aa198857d799175ffe231a6", "drivers/net/ethernet/marvell").toString();
            }
        }.start(true);
    }


    @Test
    public void testPerfDirectory() throws Exception {
        new Timed<String>("directory", 10000) {
            @Override
            protected String iteration() throws Exception {
                return directory(LINUX, "2e0cbf2cc2c9371f0aa198857d799175ffe231a6", "drivers/net/ethernet/marvell").toString();
            }
        }.start(false);
    }

    @Test
    public void testBranchesLazy() throws Exception {
        new Timed<String>("branches lazy", 10000) {
            @Override
            protected String iteration() throws Exception {
                return branches(JIRA, false).toString();
            }
        }.start(true);
    }

    @Test
    public void testBranches() throws Exception {
        new Timed<String>("branches", 10000) {
            @Override
            protected String iteration() throws Exception {
                return branches(JIRA, false).toString();
            }
        }.start(false);
    }

    public static final Pattern PATTERN_HASH = Pattern.compile("[a-z0-9]{40}");

    public static boolean isHash(String value) {
        return PATTERN_HASH.matcher(value).matches();
    }

    private String type(File repository, final String changesetId, final String path) {
        return execJGit(repository, new Function<Repository, String>() {

            public String apply(Repository jgit) {
                try {
                    RevWalk walk = new RevWalk(jgit);
                    try {
                        ObjectId commitId;
                        if (isHash(changesetId)) {
                            commitId = ObjectId.fromString(changesetId);
                        } else {
                            commitId = jgit.resolve(changesetId);
                            if (commitId == null) {
                                throw new RuntimeException("Changeset {} does not exist." + changesetId);
                            }
                        }
                        RevCommit commit = walk.parseCommit(commitId);

                        if (path == null) {
                            //If there is no path, the root of a commit is _always_ a tree. This is just deferred to
                            //here (it could be done immediately) to allow resolving the commit to ensure it exists
                            return "DIRECTORY";
                        }

                        TreeWalk tree = TreeWalk.forPath(jgit, path, commit.getTree());
                        if (tree == null) {
                            throw new RuntimeException("The requested path, {}, does not exist at revision {}.");
                        }
                        try {
                            ObjectId objectId = tree.getObjectId(0);
                            AbstractTreeIterator it = tree.getTree(tree.getTreeCount() - 1, AbstractTreeIterator.class);
                            FileMode mode = FileMode.fromBits(it.getEntryRawMode());

                            switch (mode.getObjectType()) {
                                case Constants.OBJ_BLOB:
                                    return "FILE";
                                case Constants.OBJ_COMMIT:
                                    return "SUBMODULE";
                                case Constants.OBJ_TREE:
                                    return "DIRECTORY";
                            }
                            return null;
                        } finally {
                            tree.release();
                        }
                    } finally {
                        walk.dispose();
                    }
                } catch (IOException e) {
                    throw new RuntimeException("The object type for {}@{} could not be determined.", e);
                }
            }
        });
    }

    private List<Object> directory(File dir, final String changesetId, final String path) {
        return execJGit(dir, new Function<Repository, List<Object>>() {
            public List<Object> apply(Repository jgit) {
                try {
                    final RevWalk walk = new RevWalk(jgit);
                    try {
                        ObjectId commitId;
                        if (isHash(changesetId)) {
                            commitId = ObjectId.fromString(changesetId);
                        } else {
                            commitId = jgit.resolve(changesetId);
                            if (commitId == null) {
                                throw new RuntimeException("Changeset {} does not exist." + changesetId);
                            }
                        }
                        RevCommit commit = walk.parseCommit(commitId);

                        TreeWalk tree;
                        if (path != null) {
                            tree = TreeWalk.forPath(jgit, path, commit.getTree());
                            if (tree == null) {
                                throw new RuntimeException("The requested path, {}, does not exist at revision {}." + path + changesetId);
                            }
                            tree.enterSubtree();
                        } else {
                            tree = new TreeWalk(jgit);
                            tree.addTree(commit.getTree());
                        }

                        try {
                            List<Object> l = new ArrayList<Object>();
                            while (tree.next()) {
                                ObjectId objectId = tree.getObjectId(0);
                                String path = tree.getNameString();
                                AbstractTreeIterator it = tree.getTree(tree.getTreeCount() - 1, AbstractTreeIterator.class);
                                FileMode mode = FileMode.fromBits(it.getEntryRawMode());
                                l.add(objectId + path + mode);
                            }
                            return l;
                        } finally {
                            tree.release();
                        }
                    } finally {
                        walk.dispose();
                    }
                } catch (IOException e) {
                    throw new RuntimeException("The object type for {0}@{1} could not be determined." + path + changesetId);
                }
            }
        });
        
    }

    private Object branches(File repository, final boolean alpha) {
        return execJGit(repository, new Function<Repository, Object>() {
            public Object apply(Repository jgit) {

                RefDatabase refDb = jgit.getRefDatabase();
                Map<String, Ref> refs;
                try {
                    refs = refDb.getRefs(Constants.R_HEADS);
                } catch (IOException e) {
                    throw new RuntimeException("Branches could not be loaded for the repository.", e);
                }

                if (refs.isEmpty()) {
                    return "";
                }

                List<Ref> heads = new ArrayList<Ref>(refs.values());
                if (alpha) {
                    Collections.sort(heads, new Comparator<Ref>() {

                        public int compare(org.eclipse.jgit.lib.Ref left, org.eclipse.jgit.lib.Ref right) {
                            return left.getName().compareTo(right.getName());
                        }
                    });
                } else {
                    final RevWalk walk = new RevWalk(jgit);
                    // Pre-load the objects for lazy hackss
                    if (PackFile.lazy) {
                        List<AnyObjectId> ids =new ArrayList<AnyObjectId>(refs.size());
                        for (Ref ref : heads) {
                            ids.add(ref.getObjectId());
                        }
                        try {
                            walk.parseAll(ids);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    }
                    try {
                        Collections.sort(heads, new Comparator<org.eclipse.jgit.lib.Ref>() {

                            public int compare(org.eclipse.jgit.lib.Ref left, org.eclipse.jgit.lib.Ref right) {
                                RevCommit leftCommit = parse(left);
                                RevCommit rightCommit = parse(right);

                                //Sort in descending order, newest to oldest
                                return rightCommit.getAuthorIdent().getWhen().compareTo(leftCommit.getAuthorIdent().getWhen());
                            }

                            private RevCommit parse(org.eclipse.jgit.lib.Ref ref) {
                                try {
                                    return walk.parseCommit(ref.getTarget().getObjectId());
                                } catch (IOException e) {
                                    throw new RuntimeException("Commit {} for branch {} could not be parsed."+
                                            ref.getTarget().getObjectId().name() + " "+
                                            org.eclipse.jgit.lib.Repository.shortenRefName(ref.getName()));
                                }
                            }
                        });
                    } finally {
                        walk.dispose();
                    }
                }
                return heads;
            }
        });
    }

    private <T> T execJGit(final File repository, final Function<org.eclipse.jgit.lib.Repository, T> callback) {
        org.eclipse.jgit.lib.Repository jgit = null;
        try {
            try {
                jgit = new RepositoryBuilder()
                        .setBare()
                        .setGitDir(repository)
                        .setMustExist(true)
                        .build();
            } catch (IOException e) {
                throw new RuntimeException("The repository could not be accessed.", e);
            }
            return callback.apply(jgit);
        } finally {
            if (jgit != null) {
                jgit.close();
            }
        }
    }

    public static interface Function<F, T> {
        T apply(F input);

    }


    private abstract static class Timed<T> {
        private final int iterations;
        private final long maxTimeNanos;
        private final String name;

        public Timed(String name, int iterations) {
            this(name, iterations, TimeUnit.SECONDS.toMillis(5L));
        }

        public Timed(String name, int iterations, long maxTimeMillis) {
            this.iterations = iterations;
            this.maxTimeNanos = TimeUnit.MILLISECONDS.toNanos(maxTimeMillis);
            this.name = name;
        }

        public T start(boolean lazy) {
            PackFile.lazy = lazy;
            try {
                warmup();
                long start = System.nanoTime();
                int realIterations = iterate(iterations, maxTimeNanos);
                long totalTimeNanos = System.nanoTime() - start;
                System.out.println("Completed " + realIterations + " runs of [" + name + "] in " +
                        TimeUnit.NANOSECONDS.toMillis(totalTimeNanos) + "ms = " +
                        (totalTimeNanos / realIterations) / (double) TimeUnit.MILLISECONDS.toNanos(1) + "ms per run");
                T iteration = iteration();
                System.out.println("VALUE: " + iteration);
                return iteration;
            } catch (Exception e) {
                e.printStackTrace();
                Assert.fail(name + " failed with exception: " + e.getMessage());
            return null;
            } finally {
                PackFile.lazy = false;
            }
        }

        private void warmup() throws Exception {
            iterate(Integer.MAX_VALUE, TimeUnit.MILLISECONDS.toNanos(250L));
        }

        private int iterate(int iterations, long maxTimeNanos) throws Exception {
            long start = System.nanoTime();
            for (int i = 0; i < iterations; ++i) {
                iteration();
                if (System.nanoTime() - start > maxTimeNanos) {
                    return i + 1;
                }
            }
            return iterations;
        }

        protected abstract T iteration() throws Exception;
    }
}
